var fs = require('fs');
var images = require("images");
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var configDB = require('../config/database.js');
mongoose.connect(configDB.url);
var Schema = mongoose.Schema;
var client = require('twilio')('AC0e109140e52901674ea63037e2b78948', '9a2ef5d70b7af38974ee658c689e581b');
//var baseURL = 'http://46.101.220.56:3000/';
var baseURL = 'http://campaniecraciun.com:2016/';
var savedPic = "";
var dataIsProcessed = false;

var userDataSchema = new Schema({
  nume: String,
  prenume: String,  
  telefon:String
}, {collection: 'user'});

var pictureSchema = new Schema({
  userId: String,
  timestamp: Number,
  telefon: String,
  picture: String
}, {collection: 'picture'});

var UserData = mongoose.model('UserData', userDataSchema);
var PictureData = mongoose.model('PictureData', pictureSchema);

var currentUsrId = '';
var currentUsrPhone = '';

var WebSocketServer = require('ws').Server;
wss = new WebSocketServer({ port: 4000 });
// Broadcast to all.

wss.on('connection', function connection(ws) {
  ws.on('message', function message(data) {
    // Broadcast to everyone else.
    wss.clients.forEach(function each(client) {
      processData(client,data);
      client.send(data);
    });
  });
});

router.get('/', function(req, res, next) {
	
 currentUsrId = '';
 currentUsrPhone = '';
 savedPic = '';
 dataIsProcessed = false;
  res.render('index');
});

/* GET START page. */
router.get('/startPage', function(req, res, next) {
  res.render('startPage');
});

router.get('/galerie', function(req, res, next) { 
  PictureData.find({},'userId timestamp',{limit:50, sort:{timestamp:-1}},function(err, docs) {
    if (err) {
      console.error('error, no entry found');
    }
	var urlArr = [];
	
	docs.forEach(function each(item)
	{
		if(item.userId !='')
		{
			var itm = baseURL + 'images/'+ item.userId + '-frame.jpg';
        urlArr.push(itm);			
		}		
	});	
	res.render('galerie',{jsonData:urlArr});
  });  
});


router.post('/insert', function(req, res, next) {
  var item = {
    nume: req.body.nume,
    prenume: req.body.prenume,   	
    telefon:req.body.telefon
  };

  var data = new UserData(item);
  data.save();  
  currentUsrId = data._id;
  currentUsrPhone = data.telefon;  

  res.redirect('/startPage');
});

router.get('/end', function(req, res, next) {	
	
	if(savedPic != '')
	{
		 fs.unlink('public/' + savedPic + '.png');
	}

 currentUsrId = '';
 currentUsrPhone = '';
 savedPic = '';
 dataIsProcessed = false;
 res.send('ok');
});

function processData(webS,data)
{
  if (data != 'start' && data != 'end' && data != 'done' && dataIsProcessed == false)
  {
	  dataIsProcessed = true;
     var item = {
      userId: currentUsrId,
      telefon: currentUsrPhone,
	  timestamp :new Date().getTime(),
      picture: data
       };

     var dataPic = new PictureData(item);
     dataPic.save();
     
     savedPic = 'images/'+ currentUsrId;
     fs.writeFile('public/' + savedPic + '.png', data, 'base64', function(err){
      images('public/' + savedPic + '.png')                     //Load image from file 
                                          
        .size(1920, 1080)                          //Geometric scaling the image to 400 pixels width
                                            //
        .draw(images("frame.png"), 0, 0)   //Drawn logo at coordinates (10,10)
                                            
        .save('public/' + savedPic + '-frame.jpg', {               //Save the image to a file,whih quality 50
            quality : 100                    
      });
    });
	
	
	if(currentUsrPhone!= '')
	{
		var actualPhone = '+4'+ currentUsrPhone;
		  //Send an SMS text message
		client.sendMessage({
	
			to: actualPhone, // Any number Twilio can deliver to
			from: '+16318503659 ', // A number you bought from Twilio and can use for outbound communication
			body: 'Sărbători fericite!\nVezi aici poza ta din lumea lui Moș Crăciun: ' + baseURL + savedPic + '-frame.jpg' // body of the SMS message

		}, function(err, responseData) { //this function is executed when a response is received from Twilio

			if (!err) { // "err" is an error received during the request, if any       
     
			webS.send("done");
			}
		});		
	}
	else
	{
		webS.send("done");
	}	
  } 
};


router.get('/sms', function(req, res, next) {
/*
  //Send an SMS text message
client.sendMessage({

    to: currentUsrPhone, // Any number Twilio can deliver to
    from: '+16318503659 ', // A number you bought from Twilio and can use for outbound communication
    body: 'Auzi ba! Vezi ca ti-a facut unu o poza aici: ' + baseURL + savedPic + '-frame.jpg' // body of the SMS message

}, function(err, responseData) { //this function is executed when a response is received from Twilio

    if (!err) { // "err" is an error received during the request, if any

        // "responseData" is a JavaScript object containing data received from Twilio.
        // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
        // http://www.twilio.com/docs/api/rest/sending-sms#example-1

        console.log(responseData.from); // outputs "+14506667788"
        console.log(responseData.body); // outputs "word to your mother."

    }
});*/
  
});

module.exports = router;